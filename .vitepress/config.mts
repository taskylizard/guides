import { defineConfig } from 'vitepress'

export default defineConfig({
  title: "Guides",
  base: "/guides/",
  cleanUrls: true,
  outDir: "public",
  assetsDir: "pictures",
  lastUpdated: false,
  description: "Guides for the PGames discord server.",
  themeConfig: {
    search: {
      provider: 'local',
    },
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Linux', link: '/linux/1-introduction' }
    ],

    sidebar: [
      {
        text: "General", collapsed: false, items: [
          { text: "Beginners Guide", link: "/beginners-guide" },
          { text: "Browser Extensions", link: "/browser-extensions" },
          { text: "Consoles", link: "/consoles" },
          { text: "JDownloader2", link: "/jdownloader2" },
          { text: "Megathread", link: "/megathread" },
          { text: "Qbittorrent", link: "/qbittorrent" },
          { text: "cs.rin.ru Guide", link: "/rin-guide" }
        ]
      },
      {
        text: "Linux", collapsed: true, items: [
          { text: "Introduction", link: "/linux/1-introduction" },
          { text: "Steam Deck", link: "/linux/2-steam-deck" },
          { text: "How to play on Linux", link: "/linux/3-how-to-play-on-linux" },
          { text: "Database", link: "/linux/4-database" },
          { text: "Useful Resources", link: "/linux/5-useful-resources" },
          { text: "Tips and Tricks", link: "/linux/6-tips-and-tricks" }
        ]
      }
    ],
  }
})
